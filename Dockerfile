FROM nginx
COPY /html /usr/share/nginx/html
COPY /ssl /usr/share/nginx/ssl
COPY default.conf /etc/nginx/conf.d/default.conf